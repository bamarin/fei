﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;
using System.Diagnostics;


namespace PRLab.FEI
{
    [AlgorithmInfo("Estrazione contorno", Category = "FEI")]
    [BioLab.GUI.Forms.CustomAlgorithmPreviewOutput(typeof(BioLab.GUI.Forms.ContourExtractionViewer))]
    public class EstrazioneContorni : TopologyOperation<List<CityBlockContour>>
    {
        public override void Run()
        {
            Result = new List<CityBlockContour>();
            var pixelStart = new ImageCursor(InputImage);

            //Scorro l'img finché non trovo un pixel di foreground
            do
            {
                if (InputImage[pixelStart] == Foreground)
                    break;
            } while (pixelStart.MoveNext());

            //Aggiungo i pixel di contorno a c in seno antiorario
            // Parametri:
            CityBlockDirection direction = CityBlockDirection.West;
            var contour = new CityBlockContour(pixelStart.X, pixelStart.Y);
            var cursor = new ImageCursor(pixelStart); //creo una copia per capire quando ho chiuso il contorno
            do
            {
                for (int j = 1; j <= 4; j++)
                {
                    direction = CityBlockMetric.GetNextDirection(direction); //Senso anti-orario
                    if (InputImage[cursor.GetAt(direction)] == Foreground)
                    {
                        break;
                    }
                }
                contour.Add(direction); // nuova direzione aggiunta al contorno
                cursor.MoveTo(direction); // si sposta seguendo la direzione
                direction = CityBlockMetric.GetOppositeDirection(direction); // direzione
                                                                             // rispetto al pixel precedente
            } while (cursor != pixelStart); // si ferma quando incontra il pixel iniziale
            Result.Add(contour);
        }
    }






    //Soluzione
    [AlgorithmInfo("Estrazione dei contorni", Category = "FEI")]
    [BioLab.GUI.Forms.CustomAlgorithmPreviewOutput(typeof(BioLab.GUI.Forms.ContourExtractionViewer))]
    public class EstrazioneContorniSol : TopologyOperation<List<CityBlockContour>>
    {
        public override void Run()
        {
            // Per semplicità lavora su una copia dell'immagine
            // escludendo i pixel di bordo (vengono forzati a background)
            byte background = (byte)~Foreground; // ragionevole
            Image<byte> workImage = (Image<byte>)new ImageBorderFilling<byte>(InputImage, 1, background, false).Execute();

            // Utilizza un'array per tenere traccia dei pixel di bordo da visitare
            bool[] toBeVisited = new bool[workImage.PixelCount];

            // Marca tutti i pixel di bordo come da visitare
            ImageCursor pixelCursor = new ImageCursor(workImage);
            do
            {
                // Utilizza metrica d4 e cerca pixel di bordo che non siano isolati
                if (workImage[pixelCursor] == Foreground &&
                    (workImage[pixelCursor.North] != Foreground || workImage[pixelCursor.East] != Foreground ||
                     workImage[pixelCursor.West] != Foreground || workImage[pixelCursor.South] != Foreground) &&
                    (workImage[pixelCursor.North] == Foreground || workImage[pixelCursor.East] == Foreground ||
                     workImage[pixelCursor.West] == Foreground || workImage[pixelCursor.South] == Foreground))
                    toBeVisited[pixelCursor] = true;
            } while (pixelCursor.MoveNext());

            // Risultato: lista di contorni
            Result = new List<CityBlockContour>();

            // Scorre tutti i pixel da visitare
            pixelCursor.Restart();
            do
            {
                if (toBeVisited[pixelCursor])
                {
                    CityBlockContour contour = new CityBlockContour(pixelCursor.X, pixelCursor.Y);
                    FollowContour(workImage, pixelCursor, contour, toBeVisited);
                    Result.Add(contour);
                }
            } while (pixelCursor.MoveNext());
        }

        private void FollowContour(Image<byte> image, ImageCursor pixelStart, CityBlockContour contour, bool[] toBeVisited)
        {
            // Ipotesi: 
            //    - pixel di partenza appartenente a un contorno
            //    - pixel di partenza foreground, non isolato e non di bordo
            //    - nessun pixel di bordo è foreground

            Debug.Assert(pixelStart.X > 0 && pixelStart.X < image.Width - 1 && pixelStart.Y > 0 && pixelStart.Y < image.Height - 1);
            Debug.Assert(image[pixelStart] == Foreground);

            var cursor = new ImageCursor(pixelStart);
            // Ricava la direzione di "entrata", cercando un pixel di background vicino
            CityBlockDirection direction;
            for (direction = CityBlockDirection.East; direction <= CityBlockDirection.South; direction++)
            {
                if (image[cursor.GetAt(direction)] != Foreground)
                {
                    break;
                }
            }
            do
            {
                toBeVisited[cursor] = false;
                for (int j = 1; j <= 4; j++)
                {
                    direction = CityBlockMetric.GetNextDirection(direction);
                    if (image[cursor.GetAt(direction)] == Foreground)
                    {
                        break;
                    }
                }
                contour.Add(direction); // nuova direzione aggiunta al contorno
                cursor.MoveTo(direction); // si sposta seguendo la direzione
                direction = CityBlockMetric.GetOppositeDirection(direction);  // direzione rispetto al pixel precedente
            } while (cursor != pixelStart);    // si ferma quando incontra il pixel iniziale
        }
    }

}
