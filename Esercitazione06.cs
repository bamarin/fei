﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{
    [AlgorithmInfo("Estrazione bordi", Category = "FEI")]
    public class CalcolaModuloGradiente : ImageOperation<Image<byte>, Image<int>>
    {
        private ConvolutionFilter<int> xFilter, yFilter;
        private Convolution<byte, int, ConvolutionFilter<int>> xConv, yConv;

        public CalcolaModuloGradiente(Image<byte> inputImg)
        {
            int[] xVal = { 1, 0, -1, 1, 0, -1, 1, 0, -1 };
            int[] yVal = { 1, 1, 1, 0, 0, 0, -1, -1, -1 };
            InputImage = inputImg;
            xFilter = new ConvolutionFilter<int>(xVal, 3);
            yFilter = new ConvolutionFilter<int>(yVal, 3);
        }
        public override void Run()
        {
            Result = new Image<int>(InputImage.Width, InputImage.Height);
            xConv = new ConvoluzioneByteInt();
            yConv = new ConvoluzioneByteInt();
            xConv.InputImage = InputImage;
            xConv.Filter = xFilter;
            yConv.InputImage = InputImage;
            yConv.Filter = yFilter;
            Image<int> dX = xConv.Execute();
            Image<int> dY = yConv.Execute();

            for(int i=0; i<InputImage.PixelCount; i++)
            {
                Result[i] = (int) Math.Sqrt(Math.Pow(dX[i], 2) + Math.Pow(dY[i],2));
            }

            OnIntermediateResult(new AlgorithmIntermediateResultEventArgs(dX, "Gradiente X"));
            OnIntermediateResult(new AlgorithmIntermediateResultEventArgs(dY, "Gradiente Y"));
        }
    }


    [AlgorithmInfo("Trasformata distanza", Category = "FEI")]
    public class TrasformataDistanza : TopologyOperation<Image<int>>
    {
        public TrasformataDistanza()
        {
        }

        public TrasformataDistanza(Image<byte> inputImage, byte foreground, MetricType metric)
          : base(inputImage, foreground)
        {
            Metric = metric;
        }

        private int Min(int a, int b)
        {
            return a < b ? a : b;
        }
        private int Min(int a, int b, int c)
        {
            var m = Min(a, b);
            return m < c ? m : c;
        }
        private int Min(int a, int b, int c, int d)
        {
            var m1 = Min(a, b);
            var m2 = Min(c, d);
            return m1 < m2 ? m1 : m2;
        }
        private int Min(int a, int b, int c, int d, int e)
        {
            var m1 = Min(a, b);
            var m2 = Min(c, d, e);
            return m1 < m2 ? m1 : m2;
        }

        public override void Run()
        {
            Result = new Image<int>(InputImage.Width, InputImage.Height);
            var r = Result;
            var cursor = new ImageCursor(r, 1);
            if (Metric == MetricType.CityBlock)
            {
                do // Scansione diretta
                {
                    if (InputImage[cursor] == Foreground)
                    {
                        r[cursor] = Min(r[cursor.West], r[cursor.North]) + 1;
                    }
                } while (cursor.MoveNext());

                do // Scansione inversa
                {
                    if (InputImage[cursor] == Foreground)
                    {
                        r[cursor] = Min(r[cursor.East] + 1, r[cursor.South] + 1, r[cursor]);
                    }
                } while (cursor.MovePrevious());
            }
            else
            {
                do // Scansione diretta
                {
                    if (InputImage[cursor] == Foreground)
                    {
                        r[cursor] = Min(r[cursor.Northwest], r[cursor.North], r[cursor.Northeast], r[cursor.West]) + 1;
                    }
                } while (cursor.MoveNext());

                do // Scansione inversa
                {
                    if (InputImage[cursor] == Foreground)
                    {
                        r[cursor] = Min(r[cursor.Southeast] +1, r[cursor.South] +1, r[cursor.Southwest] +1, r[cursor.East] +1, r[cursor]);
                    }
                } while (cursor.MovePrevious());
            }
        }

        [AlgorithmParameter]
        public MetricType Metric { get; set; }
    }


}
