﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;

namespace PRLab.FEI
{

    [AlgorithmInfo("Negativo Grayscale", Category = "FEI")]
    public class NegativeImage : ImageOperation<Image<byte>, Image<byte>>
    {
        public override void Run()
        {
            Result = new Image<byte>(InputImage.Width, InputImage.Height);
            //TODO: impostare l'immagine Result come negativo dell'immagine InputImage
            for (int i = 0; i < Result.PixelCount; i++){
                Result[i] = (Byte)(0xff - InputImage[i]);
            }
        }
    }

    [AlgorithmInfo("Negativo RGB", Category = "FEI")]
    public class NegativeRgbImage : ImageOperation<RgbImage<byte>, RgbImage<byte>>
    {
        public override void Run()
        {
            Result = new RgbImage<byte>(InputImage.Width, InputImage.Height);

            for (int i = 0; i < Result.PixelCount; i++)
            {
                Result.RedChannel[i] = (byte)(0xff - InputImage.RedChannel[i]);
                Result.GreenChannel[i] = (byte)(0xff - InputImage.GreenChannel[i]);
                Result.BlueChannel[i] = (byte)(0xff - InputImage.BlueChannel[i]);
            }
        }
    }

    [AlgorithmInfo("Luminosità", Category = "FEI")]
    public class Brightness : ImageOperation<Image<byte>, Image<byte>>
    {
        [AlgorithmParameter]
        public int Variazione {get; set;}

        public override void Run()
        {
            PixelMapping<byte, byte> f = p => (p + Variazione * 255 / 100).ClipToByte();
            Result = new LookupTableTransform<byte>(InputImage, f).Execute();
        }
    }

    [AlgorithmInfo("Pseudocolors", Category = "FEI")]
    public class Pseudocolors : ImageOperation<Image<byte>, RgbImage<byte>>
    {

        public override void Run()
        {
            var tmp = new HslImage<byte>(InputImage.Width, InputImage.Height);
            for (int i = 0; i < tmp.PixelCount; i++)
            {
                tmp.HueChannel[i] = InputImage[i];
                tmp.SaturationChannel[i] = 0xff;
                tmp.LightnessChannel[i] = 0x88;
            }
            Result = tmp.ToByteRgbImage();

        }
    }


}
