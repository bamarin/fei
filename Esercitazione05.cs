﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{
    
    [AlgorithmInfo("Convoluzione (da immagine byte a immagine int)", Category = "FEI")]
    [CustomAlgorithmPreviewParameterControl(typeof(SimpleConvolutionParameterControl))]
    public class ConvoluzioneByteInt : Convolution<byte, int, ConvolutionFilter<int>>
    {
        public override void Run()
        {
            Result = new Image<int>(InputImage.Width, InputImage.Height);
            int w = InputImage.Width;
            int h = InputImage.Height;
            int m = Filter.Size;
            int m2 = m / 2;
            int i1 = m2;
            int i2 = h - m2 - 1;
            int j1 = m2;
            int j2 = w - m2 - 1;
            int nM = Filter.Size * Filter.Size;
            int[] FOff = new int[nM];
            int[] FVal = new int[nM];
            int maskLen = 0;


            for (int y = 0; y < Filter.Size; y++)
                for (int x = 0; x < Filter.Size; x++)
                    if (Filter[y, x] != 0)
                    {
                        FOff[maskLen] = (m2 - y) * w + (m2 - x);
                        FVal[maskLen] = Filter[y, x]; maskLen++;
                    }
            int index = m2 * (w + 1); // indice lineare all'interno dell'immagine
            int indexStepRow = m2 * 2; // aggiustamento indice a fine riga (salta bordi)
            for (int y = i1; y <= i2; y++, index += indexStepRow)
                for (int x = j1; x <= j2; x++)
                {
                    int val = 0;
                    for (int k = 0; k < maskLen; k++)
                        val += InputImage[index + FOff[k]] * FVal[k];
                    Result[index++] = val / Filter.Denominator;
                }


        }

    }

    [AlgorithmInfo("Sfocatura", Category = "FEI")]
    public class Sfocatura : ImageOperation<Image<byte>, Image<byte>>
    {
        [AlgorithmParameter]
        public int FilterSize { get; set; }
        public override void Run()
        {
            Result = new Image<byte>(InputImage.Width, InputImage.Height);
            var conv = new ConvoluzioneByteInt();
            conv.Filter = new ConvolutionFilter<int>(FilterSize, FilterSize * FilterSize);
            for (int i = 0; i < FilterSize * FilterSize; i++)
            {
                conv.Filter[i] = 1;
            }
            conv.InputImage = InputImage;
            var intResult = conv.Execute();
            for (int i = 0; i < intResult.PixelCount; i++)
            {
                Result[i] = intResult[i].ClipToByte();
            }

        }
    }

    [AlgorithmInfo("Nitidezza", Category = "FEI")]
    public class Nitidezza : ImageOperation<Image<byte>, Image<byte>>
    {
        [AlgorithmParameter]
        public double Weight { get; set; }
        public override void Run()
        {
            Result = InputImage.Clone();
            var conv = new ConvoluzioneByteInt();
            int[] values = { -1, -1, -1, -1, 8, -1, -1, -1, -1 };
            conv.Filter = new ConvolutionFilter<int>(values, 1);
            conv.InputImage = InputImage;
            var intResult = conv.Execute();
            var mask = new Image<byte>(InputImage.Width, InputImage.Height);
            for (int i = 0; i < intResult.PixelCount; i++)
            {
                Result[i] += (Weight * intResult[i]).RoundAndClipToByte();
            }
            


        }
    }


}
