﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI
{
    

    //Es1
    [AlgorithmInfo("Calcolo Istogramma", Category = "FEI")]
    public class HistogramBuilder : ImageOperation<Image<byte>, Histogram>
    {
        [AlgorithmParameter]
        [DefaultValue(false)]
        public bool Sqrt { get; set; }

        [AlgorithmParameter]
        [DefaultValue(0)]
        public int SmoothWindowSize { get; set; }

        public HistogramBuilder(Image<byte> inputImage)
        {
            InputImage = inputImage;
        }

        public HistogramBuilder(Image<byte> inputImage, bool sqrt, int smoothWindowSize)
        {
            InputImage = inputImage;
            Sqrt = sqrt;
            SmoothWindowSize = smoothWindowSize;
        }

        public override void Run()
        {
            Result = new Histogram();
            foreach (byte pixel in InputImage)
            {
                Result[pixel]++;
            }

            if (Sqrt)
            {
                for (int i = 0; i <= 255; i++)
                {
                    Result[i] = (int)Math.Sqrt(Result[i]);
                }
            }

            if (SmoothWindowSize > 0)
            {
                int i, j, sum;
                Histogram res = new Histogram();
                for (i = 0; i <= 255; i++)
                {
                    sum = 0;
                    if (i >= SmoothWindowSize && i <= 255 - SmoothWindowSize)
                    {
                        for (j = -SmoothWindowSize; j <= SmoothWindowSize; j++)
                            sum += Result[i + j];
                    }
                    res[i] = sum / (2 * SmoothWindowSize + 1);
                }
                Result = res;
            }
        }
    }

    //Es2
    [AlgorithmInfo("Binarizzazione", Category = "FEI")]
    public class Binarify : ImageOperation<Image<byte>, Image<byte>>
    {
        [AlgorithmParameter]
        [DefaultValue(0)]
        public int Threshold
        {
            get { return thr; }
            set
            {
                if (value < 0)
                    thr = 0;
                else if (value >= 0 && value <= 255)
                {
                    thr = value;
                }
                else thr = 255;
            }
        }

        public override void Run()
        {
            Result = new Image<byte>(InputImage.Width, InputImage.Height);
            for (int i = 0; i < InputImage.PixelCount; i++)
            {
                if (InputImage[i] <= thr)
                    Result[i] = 0;
                else
                    Result[i] = 255;
            }
        }

        private int thr;
    }

    //Es3
    [AlgorithmInfo("Separa Canali", Category = "FEI")]
    public class Canali : Algorithm
    {
        [AlgorithmInput]
        public RgbImage<byte> InputImage { get; set; }
        [AlgorithmOutput]
        public Image<byte> Rosso { get; protected set; }
        [AlgorithmOutput]
        public Image<byte> Verde { get; protected set; }
        [AlgorithmOutput]
        public Image<byte> Blu { get; protected set; }

        public override void Run()
        {
            Rosso = InputImage.RedChannel.Clone();
            Verde = InputImage.GreenChannel.Clone();
            Blu = InputImage.BlueChannel.Clone();
        }
    }

    [AlgorithmInfo("Combina Canali", Category = "FEI")]
    public class Combine : Algorithm
    {
        [AlgorithmOutput]
        public RgbImage<byte> OutputImage { get; protected set; }
        [AlgorithmInput]
        public Image<byte> Rosso { get; set; }
        [AlgorithmInput]
        public Image<byte> Verde { get; set; }
        [AlgorithmInput]
        public Image<byte> Blu { get; set; }

        public override void Run()
        {
            OutputImage = new RgbImage<byte>(Rosso, Verde, Blu);
        }
    }

    [AlgorithmInfo("Luminosità (RGB)", Category = "FEI")]
    public class Lum : ImageOperation<RgbImage<byte>, RgbImage<byte>>
    {
        [AlgorithmParameter]
        [DefaultValue(0)]
        public double Variazione
        {
            get
            {
                return val;
            }
            set
            {
                val = value;
                if (val < 0) val = 0;
                else if (val > 1) val = 1;
            }
        }
        public override void Run()
        {
            var r = new Image<byte>(InputImage.Width, InputImage.Height);
            var g = new Image<byte>(InputImage.Width, InputImage.Height);
            var b = new Image<byte>(InputImage.Width, InputImage.Height);
            PixelMapping<byte, byte> f = p => ((int)(p + val * 255)).ClipToByte();
            r = new LookupTableTransform<byte>(InputImage.RedChannel, f).Execute();
            g = new LookupTableTransform<byte>(InputImage.GreenChannel, f).Execute();
            b = new LookupTableTransform<byte>(InputImage.BlueChannel, f).Execute();
            Result = new RgbImage<byte>(r, g, b);
        }

        private double val;
    }

    [AlgorithmInfo("RGB2HSL", Category = "FEI")]
    public class RGB2HSL : Algorithm
    {
        [AlgorithmInput]
        public RgbImage<byte> InputImage { get; set; }
        [AlgorithmOutput]
        public Image<byte> Tono { get; protected set; }
        [AlgorithmOutput]
        public Image<byte> Sat { get; protected set; }
        [AlgorithmOutput]
        public Image<byte> Luce { get; protected set; }

        public override void Run()
        {
            var hsl = InputImage.ToByteHslImage();
            Tono = hsl.HueChannel;
            Sat = hsl.SaturationChannel;
            Luce = hsl.LightnessChannel;
        }
    }

    //Facoltativo 1
    [AlgorithmInfo("crea HSL", Category = "FEI")]
    public class HueSatLight : Algorithm
    {
        [AlgorithmOutput]
        public RgbImage<byte> OutputImage { get; protected set; }
        [AlgorithmInput]
        public Image<byte> Tono { get; set; }
        [AlgorithmInput]
        public Image<byte> Sat { get; set; }
        [AlgorithmInput]
        public Image<byte> Luce { get; set; }

        public override void Run()
        {
            var hsl = new HslImage<byte>(Tono, Sat, Luce);
            OutputImage = hsl.ToByteRgbImage();
        }
    }

    //Facoltativo 2
    [AlgorithmInfo("Colora", Category = "FEI")]
    public class Colora : ImageOperation<Image<byte>,RgbImage<byte>>
    {
        [AlgorithmParameter]
        public int Hue { get; set; }
        [AlgorithmParameter]
        public int Sat { get; set; }

        public override void Run()
        {
            var h = new Image<byte>(InputImage.Width, InputImage.Height, Hue.ClipToByte());
            var s = new Image<byte>(InputImage.Width, InputImage.Height, Sat.ClipToByte());
            Result = new HslImage<byte>(h, s, InputImage).ToByteRgbImage();
        }
    }


    

}
