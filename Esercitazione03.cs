﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI
{

    [AlgorithmInfo("Stretch del contrasto", Category = "FEI")]
    public class ContrastStretch : ImageOperation<Image<byte>, Image<byte>>
    {
        private int stretchDiscardPerc;

        [AlgorithmParameter]
        [DefaultValue(0)]
        public int StretchDiscardPercentage
        {
            get { return stretchDiscardPerc; }
            set
            {
                if (value < 0 || value > 100)
                    throw new ArgumentOutOfRangeException("Inserire un valore fra 0 and 100.");
                stretchDiscardPerc = value;
            }
        }

        public ContrastStretch()
        {
        }

        public ContrastStretch(Image<byte> inputImage)
          : base(inputImage)
        {
            StretchDiscardPercentage = 0;
        }

        public ContrastStretch(Image<byte> inputImage, int stretchDiscard)
          : base(inputImage)
        {
            StretchDiscardPercentage = stretchDiscard;
        }

        public override void Run()
        {
            var istogramma = new HistogramBuilder(InputImage).Execute();
            int min = -1, max = 256;

            for (int i = 0, cmin = 0, cmax = 0; i <= 255 && (min < 0 || max > 255); i++)
            {
                cmin += istogramma[i];
                cmax += istogramma[255 - i];
                if (cmin > InputImage.PixelCount * StretchDiscardPercentage / 100 && min < 0) min = i;
                if (cmax > InputImage.PixelCount * StretchDiscardPercentage / 100 && max > 255) max = 255 - i;
            }
            int diff = max - min;
            if (diff > 0)
            {
                var op = new LookupTableTransform<byte>(InputImage,
                p => (255 * (p - min) / diff).ClipToByte());
                Result = op.Execute();
            }
            else Result = InputImage.Clone();
        }
    }


    [AlgorithmInfo("Equalizzazione istogramma", Category = "FEI")]
    public class HistogramEqualization : ImageOperation<Image<byte>, Image<byte>>
    {
        public HistogramEqualization()
        {
        }

        public HistogramEqualization(Image<byte> inputImage)
          : base(inputImage)
        {
        }

        public override void Run()
        {
            // Calcola l'istogramma
            var hist = new HistogramBuilder(InputImage).Execute();
            // Ricalcola ogni elemento dell'istogramma come somma dei precedenti
            for (int i = 1; i < 256; i++)
                hist[i] += hist[i - 1];
            // Definisce la funzione di mapping e applica la LUT
            var op = new LookupTableTransform<byte>(InputImage,
             p => (byte)(255 * hist[p] / InputImage.PixelCount));
            Result = op.Execute();

        }
    }

    [AlgorithmInfo("Operazione aritmetica", Category = "FEI")]
    public class ImageArithmetic : ImageOperation<Image<byte>, Image<byte>, Image<byte>>
    {
        [AlgorithmParameter]
        [DefaultValue(defaultOperation)]
        public ImageArithmeticOperation Operation { get; set; }
        const ImageArithmeticOperation defaultOperation = ImageArithmeticOperation.Difference;

        public ImageArithmetic()
        {
        }

        public ImageArithmetic(Image<byte> image1, Image<byte> image2, ImageArithmeticOperation operation)
          : base(image1, image2)
        {
            Operation = operation;
        }

        public ImageArithmetic(Image<byte> image1, Image<byte> image2)
          : this(image1, image2, defaultOperation)
        {
        }

        public override void Run()
        {
            Result = InputImage1.Clone();
            switch (Operation)
            {
                case ImageArithmeticOperation.Add:
                    for (int i = 0; i < Result.PixelCount; i++)
                    {
                        Result[i] = (InputImage1[i] + InputImage2[i]).ClipToByte();
                    }
                    break;
                case ImageArithmeticOperation.And:
                    for (int i = 0; i < Result.PixelCount; i++)
                    {
                        Result[i] &= InputImage2[i];
                    }
                    break;
                case ImageArithmeticOperation.Average:
                    for (int i = 0; i < Result.PixelCount; i++)
                    {
                        Result[i] = ((InputImage1[i] + InputImage2[i]) / 2).ClipToByte();
                    }
                    break;
                case ImageArithmeticOperation.Darkest:
                    for (int i = 0; i < Result.PixelCount; i++)
                    {
                        if(InputImage2[i] < Result[i])
                            Result[i] = InputImage2[i];
                    }
                    break;
                case ImageArithmeticOperation.Difference:
                    for (int i = 0; i < Result.PixelCount; i++)
                    {
                        Result[i]= Math.Abs(Result[i] - InputImage2[i]).ClipToByte();
                    }
                    break;
                case ImageArithmeticOperation.Lightest:
                    for (int i = 0; i < Result.PixelCount; i++)
                    {
                        if (InputImage2[i] > Result[i])
                            Result[i] = InputImage2[i];
                    }
                    break;
                case ImageArithmeticOperation.Or:
                    for (int i = 0; i < Result.PixelCount; i++)
                    {
                        Result[i] |= InputImage2[i];
                    }
                    break;
                case ImageArithmeticOperation.Subtract:
                    for (int i = 0; i < Result.PixelCount; i++)
                    {
                        Result[i] = (Result[i] - InputImage2[i]).ClipToByte();
                    }
                    break;
                case ImageArithmeticOperation.Xor:
                    for (int i = 0; i < Result.PixelCount; i++)
                    {
                        Result[i] ^= InputImage2[i];
                    }
                    break;
            }


        }
    }
}
