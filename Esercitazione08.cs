﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{
    public class EliminaPiccoleComponenti : TopologyOperation<Image<byte>>
    {
        [AlgorithmParameter]
        public byte AreaMinima { get; set; }
        public override void Run()
        {
            List<int> ettichetteDaRimuovere = new List<int>();
            var componentsImg = new EtichettaturaComponentiConnesse(InputImage, Foreground, MetricType.Chessboard).Execute().Clone();
            var info = new InformazioniComponentiConnesse();
            info.InputImage = InputImage;
            info.Foreground = Foreground;
            
            for(int i=0; i<componentsImg.ComponentCount; i++)
            {
                if (info.Componenti[i] < AreaMinima)
                {
                    
                }
            }
            
        }
    }


    [AlgorithmInfo("Informazioni delle componenti connesse", Category = "FEI")]
    public class InformazioniComponentiConnesse : Algorithm
    {
        private int[] componentsArea;
        private EstrazioneContorniSol contorni;
        private EtichettaturaComponentiConnesse ecc;

        [AlgorithmInput]
        public Image<byte> InputImage { get; set; }
        [AlgorithmParameter]
        public byte Foreground
        { get; set; }

        [AlgorithmOutput]
        public int numComponenti { get; private set; }
        [AlgorithmOutput]
        public int MinArea { get; private set; }
        [AlgorithmOutput]
        public double AvgArea { get; private set; }
        [AlgorithmOutput]
        public int MaxArea { get; private set; }
        [AlgorithmOutput]
        public double Avg2P { get; private set; }

        public int[] Componenti
        {
            get{ return componentsArea; }
        } 


        public override void Run()
        {
            ecc = new EtichettaturaComponentiConnesse(InputImage, Foreground, MetricType.Chessboard);
            numComponenti = ecc.Execute().ComponentCount;

            componentsArea = new int[numComponenti];
            for (int i = 0; i < ecc.Result.PixelCount; i++)
            {
                if (ecc.Result[i] >= 0)
                    componentsArea[ecc.Result[i]]++;
            }

            MinArea = componentsArea.Min();
            MaxArea = componentsArea.Max();
            AvgArea = componentsArea.Average();

            contorni = new EstrazioneContorniSol();
            contorni.InputImage = InputImage;
            contorni.Foreground = Foreground;
            List<CityBlockContour> perimetri = contorni.Execute();
            int sum = 0;
            foreach (var p in perimetri)
            {
                foreach (var x in p)
                {
                    sum++;
                }
            }

            Avg2P = sum / numComponenti;


        }
    }




    [AlgorithmInfo("Etichettatura delle componenti connesse", Category = "FEI")]
    public class EtichettaturaComponentiConnesse : TopologyOperation<ConnectedComponentImage>
    {
        public EtichettaturaComponentiConnesse(Image<byte> inputImage, byte foreground, MetricType metric)
          : base(inputImage, foreground)
        {
            Metric = metric;
        }

        public EtichettaturaComponentiConnesse(Image<byte> inputImage)
          : this(inputImage, 255, MetricType.Chessboard)
        {
        }

        public EtichettaturaComponentiConnesse()
        {
            Metric = MetricType.Chessboard;
        }

        public override void Run()
        {
            Result = new ConnectedComponentImage(InputImage.Width, InputImage.Height, -1);
            var cursor = new ImageCursor(InputImage, 1); // per semplicità ignora i bordi (1 pixel)            
            int[] neighborLabels = new int[Metric == MetricType.CityBlock ? 2 : 4];
            int nextLabel = 0;
            var equivalences = new DisjointSets(InputImage.PixelCount);
            do
            { // prima scansione
                if (InputImage[cursor] == Foreground)
                {
                    int labelCount = 0;
                    if (Result[cursor.West] >= 0) neighborLabels[labelCount++] = Result[cursor.West];
                    if (Result[cursor.North] >= 0) neighborLabels[labelCount++] = Result[cursor.North];
                    if (Metric == MetricType.Chessboard)
                    {   // anche le diagonali
                        if (Result[cursor.Northwest] >= 0) neighborLabels[labelCount++] = Result[cursor.Northwest];
                        if (Result[cursor.Northeast] >= 0) neighborLabels[labelCount++] = Result[cursor.Northeast];
                    }
                    if (labelCount == 0)
                    {
                        equivalences.MakeSet(nextLabel); // crea un nuovo set
                        Result[cursor] = nextLabel++; // le etichette iniziano da 0
                    }
                    else
                    {
                        int l = Result[cursor] = neighborLabels[0]; // seleziona la prima
                        for (int i = 1; i < labelCount; i++) // equivalenze
                            if (neighborLabels[i] != l)
                                equivalences.MakeUnion(neighborLabels[i], l); // le rende equivalenti
                    }
                }
            } while (cursor.MoveNext());


            // rende le etichette numeri consecutivi
            int totalLabels;
            int[] corresp = equivalences.Renumber(nextLabel, out totalLabels);
            // seconda e ultima scansione
            cursor.Restart();
            do
            {
                int l = Result[cursor];
                if (l >= 0)
                {
                    Result[cursor] = corresp[l];
                }
            } while (cursor.MoveNext());
            Result.ComponentCount = totalLabels;

        }

        [AlgorithmParameter]
        [DefaultValue(MetricType.Chessboard)]
        public MetricType Metric { get; set; }
    }


}
